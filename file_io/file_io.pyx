# cython: boundscheck=True, wraparound=True, nonecheck=True, language_level=3str

cimport cython
import re
import numpy as np


def read_propertyfile(fnm):
    """
    Input:  name of file containing masses or charges
    Output: dict of dicts with structure: dict[selname][atomname]
    File format: See topology/default_charges.dat for example:
    [ name of selection ] # comment 1
    atomname1 value1      # comment 2
    atomname2 value2
    """
    d = {}
    if fnm is None:
        return d
    print("Reading property file '%s'" % fnm)
    with open(fnm, "r") as f:
        for i, line in enumerate(f):
            line = line.strip()
            # Skip lines that are empty or contain only spaces and a comment
            if re.fullmatch('\s*(\#.*)?', line):
                continue
            # Check if line contains '[ groupname ]'
            match = re.fullmatch('\s*\[\s*(?P<groupname>[^\s\\#\\[\\]][^\\#\\[\\]]*?[^\s\\#\\[\\]])\s*\]\s*(\#.*)?', line)
            if match:
                groupname = match.group('groupname')
                d[groupname] = {}
            else:
                match = re.fullmatch('\s*(?P<atomname>[^\s\\#\\[\\]]*)\s*(?P<value>[+-]?\d+(\.\d+)?)\s*(\#.*)?', line)
                assert match, "Wrong format in file '%s' in line %i: '%s'" % (fnm, i + 1, line)
                atomname = match.group('atomname')
                assert atomname not in d[groupname], "File %s: Duplicate assignment of atom '%s' in group '%s'" % (fnm, atomname, groupname)
                value = float(match.group('value'))
                d[groupname][atomname] = value
    return d


def read_ndx(ndx):
    """
    Read GROMACS index file (.ndx) format.
    Input: ndx: file name
    Output: dict of lists. key: index group name. values: atom index list
    """
    print("Reading index file '%s'" % ndx)
    index = {}
    groupname = None
    with open(ndx, "r") as f:
        for i, line in enumerate(f):
            match = re.fullmatch('\s*\[\s*(?P<groupname>[^\s\\#\\[\\]][^\\#\\[\\]]*?[^\s\\#\\[\\]])\s*\]\s*', line)
            if match:
                if len(index) > 0:
                    print("Index group %7i %15s with %7i atoms" % (len(index) - 1, groupname, len(index[groupname])))
                groupname = match.group('groupname')
                index[groupname] = []  # start new index group
            else:
                assert groupname is not None, "Error reading index file '%s' in line %i: '%s'" % (ndx, i + 1, line)
                index[groupname].extend([int(atomindex) for atomindex in line.strip().split()])
    print("Index group %7i %15s with %7i atoms" % (len(index) - 1, groupname, len(index[groupname])))
    return index


def read_gro(fname):
    """read .gro file. return objects head (name and number of atoms), atoms, box size"""
    head = []
    atoms = []
    box = None
    assert fname.split('.')[-1] == 'gro', "%s is not a .gro file" % fname
    with open(fname, 'r') as f:
        head.append(next(f).rstrip().split())
        head.append(int(next(f).rstrip().split()[0]))
        N = head[-1]  # Number of atoms
        for i in range(N):
            line = next(f)
            line = [line[:5], line[5:10], line[10:15], line[15:20], line[20:28], line[28:36], line[36:44]]
            atoms.append(line)
        box = [float(i) for i in next(f).rstrip().split()]
    return [head, atoms, box]


def write_gro(head, atoms, boxsize, outf, write_mode='w'):
    """use lists head, atoms, boxsize to write in fixed gro format."""
    vel = False
    if len(atoms[0]) == 9:
        vel = True    # write velocities
    with open(outf, write_mode) as f:
        f.write('%s\n%5s\n'%(head[0], str(len(atoms))))
        for i in atoms:
            if vel:
                f.write('%5i%-5s%5s%5i%8.3f%8.3f%8.3f%8.4f%8.4f%8.4f\n' % (
                    int(i[0]), i[1], i[2], int(i[3]),
                    round(float(i[4]), 3), round(float(i[5]), 3), round(float(i[6]), 3),
                    round(float(i[7]), 4), round(float(i[8]), 4), round(float(i[9]), 4)))
            else:
                f.write('%5i%-5s%5s%5i%8.3f%8.3f%8.3f\n' % (int(i[0]), i[1], i[2], int(i[3]),
                    round(float(i[4]), 3), round(float(i[5]), 3), round(float(i[6]), 3)))

        f.write('%10s%10s%10s\n' % tuple([str(round(float(i), 3)) for i in boxsize]))


def read_xvg(filename, dtype=float):
    """ Read xmgrace file format and skip comments and grace settings """
    def iter_func():
        with open(filename, 'r') as infile:
            for line in infile:
                if "#" not in line and "@" not in line:
                    line = line.rstrip().split()
                    for item in line:
                        yield dtype(item)
    data = np.fromiter(iter_func(), dtype=dtype)
    return data
