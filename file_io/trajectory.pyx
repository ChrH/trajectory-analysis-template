# cython: boundscheck=False, wraparound=False, nonecheck=False, language_level=3str

#cimport cython
cimport openmp
from libc.stdio cimport *
from cpython.mem cimport PyMem_Malloc, PyMem_Free
from file_io.file_io import read_gro


# Import headers from xdrlib (for reading .xtc/.trr)
cdef extern from "file_io/xdrfile/xdrfile.h":
    ctypedef struct XDRFILE
    ctypedef enum:
        exdrOK
        exdrHEADER
        exdrSTRING
        exdrDOUBLE
        exdrINT
        exdrFLOAT
        exdrUINT
        exdr3DX
        exdrCLOSE
        exdrMAGIC
        exdrNOMEM
        exdrENDOFFILE
        exdrFILENOTFOUND
        exdrNR
    XDRFILE* xdrfile_open(char* path, char* mode)
    int xdrfile_close(XDRFILE* xfp)


cdef extern from "file_io/xdrfile/xdrfile_xtc.h":
    int read_xtc_natoms(char* fn, int *natoms)
    int read_xtc(XDRFILE* xd, int natoms, int* step, float* time, matrix box, rvec* x, float* prec) nogil
    int write_xtc(XDRFILE* xd, int natoms, int step, float time, matrix box, rvec* x, float prec)


cdef extern from "file_io/xdrfile/xdrfile_trr.h":
    int read_trr_natoms(char* fn, int* natoms)
    int read_trr(XDRFILE* xd, int natoms, int* step, float* t, float* lamb, matrix box, rvec* x, rvec* v, rvec* f) nogil
    int write_trr(XDRFILE* xd, int natoms, int step, float t, float lamb, matrix box, rvec* x, rvec* v, rvec* f)


cdef class Trajectory:
    """
    Inputs:
    argparse_args (dict)
    kwargs (dict)
    kwargs takes precedence over argparse_args
    """
    def __init__(self, argparse_args, **kwargs):
        self.args = argparse_args
        self.args.update(kwargs)  # overwrites entries in argparse_args if they exist in kwargs
        self.status = exdrOK
        self.end_is_set = 0
        self.lamb = 1.0  # Lambda, needed for reading trr, otherwise irrelevant (used in thermodynamic integration).
        self.file_end_status = exdrENDOFFILE
        self.mode = self.args.get('mode', 'rb')
        self.filename = self.args.get('f')

        openmp.omp_init_lock(&self.trajfile_lock)  # Initialize the lock used when reading/writing the trajectory file

        # Determine file type from extension
        self.is_trr = 0
        ftype = self.filename.split(".")
        self.ftype = ftype[len(ftype) - 1]
        assert (self.ftype == 'trr') or (self.ftype =='xtc'), "Trajectory format %s (determined from file extension) is not supported." % self.ftype
        if self.ftype == 'trr':
            self.is_trr = 1
            # Apparently, reaching the end of a .trr file returns exdrINT and not exdrENDOFFILE.
            self.file_end_status = exdrINT

        # Check if file extension is compatible with precision: single for .xtc
        if self.ftype == 'xtc' and sizeof(real) != 4:
            raise Exception("Trajectory file format is 'xtc' but sizeof(real) != 4.")

        print("Initializing trajectory from file '%s' of type '%s' in mode '%s'" % (self.filename, self.ftype, self.mode))

        # Assign the number of frames to skip between frames
        framestride = self.args.get('framestride')
        if framestride is not None:
            self.framestride = framestride
            assert self.framestride > 0, "-framestride needs to be > 0."
        else:
            self.framestride = 1

        # Stop here if not reading from the trajectory
        if self.mode != 'rb':
            return

        self._get_natoms()
        self.x = <rvec*> PyMem_Malloc(self.natoms * sizeof(rvec))
        self.v = <rvec*> PyMem_Malloc(self.natoms * sizeof(rvec))
        self.f = <rvec*> PyMem_Malloc(self.natoms * sizeof(rvec))

        # Read the first 2 frames to get properties
        self.open()
        self._read_frame_serial(&self.step, self.b, self.x, self.v, self.f)

        self.t0 = self.time
        self.s0 = self.step

        self._read_frame_serial(&self.step, self.b, self.x, self.v, self.f)

        self.dt_frame = self.time - self.t0 # Time between frames
        printf("Time step between frames = %.3e ps\n", self.dt_frame)
        self.dt = self.dt_frame * self.framestride # Time between frames to analyze with -framestride included
        self.ds = self.step - self.s0
        self.close()

        printf("With skipping every -framestride frames, the time step is %.3e ps\n", self.dt)

        # Initialize/check -b argument and calculate number of frames to skip
        b = self.args.get('b')
        if b is None:
            self.begin = self.t0
            self.skip = 0
        else:
            self.begin = b
            assert self.begin >= self.t0, "-b argument is smaller than the time of the first frame."
            # Recast result of float operations to int, works for positive ints
            self.skip = <int>((self.begin - self.t0) / self.dt_frame + 0.5)
            # Update index of first frame for correct frame indexing
            self.s0 += self.ds * self.skip
            print("Skipping the first %i frames" % self.skip)

        # Calculate last frame index from -e argument (in ps)
        e = self.args.get('e')
        if e is not None:
            assert e > self.begin, "-e argument has to be greater than -b argument (or 0)"
            self.end_is_set = 1
            # This correctly casts a positive float to integer
            self.endstep = <int> ((e - self.t0) / self.dt_frame + 0.5)
            #print("Last frame to analyze: %i" % self.endstep)

        # read .gro file
        s = self.args.get('s')
        if s is not None:
            self.grohead, self.atoms, self.boxsize = read_gro(s)
            self.natoms_gro = len(self.atoms)
            assert self.natoms == self.natoms_gro, "Files '%s' (%i) and '%s' (%i) have different numbers of atoms" \
                    % (s ,self.natoms_gro, self.filename, self.natoms)


    def _get_natoms(self):
        if self.is_trr:
            read_trr_natoms(self.filename.encode("UTF-8"), &self.natoms)
        else:
            read_xtc_natoms(self.filename.encode("UTF-8"), &self.natoms)


    def get_nframes(self):
        assert self.mode == 'rb', "Trajectory is not in reading mode"
        # If exact number of frames is known, -nframes saves time.
        nframes = self.args.get('nframes')
        if nframes is not None:
            assert not self.end_is_set, "Can not use both -nframes and -e."
            self.nframes = nframes
        else:
            self.nframes = -1  # If file end is reached, 1 is still added
            print("\nReading trajectory to get the number of frames. This may take a while...\n")
            self.open()
            self.skip_frames()
            self.status = exdrOK
            while self.status == exdrOK and (not self.end_is_set or self.step // self.ds <= self.endstep):
                if ((self.step - self.s0) // self.ds) % self.framestride == 0:
                    self.nframes += 1
                self._read_frame_serial(&self.step, self.b, self.x, self.v, self.f)
            if (not self.end_is_set and self.status != self.file_end_status) or (self.end_is_set and self.step // self.ds != self.endstep + 1):
                raise IOError("Error while skipping frames at frame %i." % self.nframes)
            self.close()
        print("Number of frames to analyze: %10i" % self.nframes)


    def skip_frames(self):
        for _ in range(self.skip):
            self._read_frame_serial(&self.step, self.b, self.x, self.v, self.f)


    cdef int _read_frame_serial(self, int* stepptr, matrix boxptr, rvec* coordptr, rvec* velptr, rvec* forceptr) except -1:
        """
        This function should not be used outside of __init__, since it is not thread-safe.
        """
        if self.is_trr:
            self.status = read_trr(self.trjfile, self.natoms, stepptr, &self.time, &self.lamb,
                                   boxptr, coordptr, velptr, forceptr)
        else:
            self.status = read_xtc(self.trjfile, self.natoms, stepptr, &self.time, boxptr, coordptr, &self.prec)
        if self.status != exdrOK and self.status != self.file_end_status:
            raise IOError("Error while reading frame %i (%.3f ps)." % (self.nframes, self.time))
        return self.status


    cdef int read_frame(self, long* t_ptr, matrix boxptr, rvec* coordptr, rvec* velptr, rvec* forceptr) nogil except -1:
        """
        This function should be used to read frames from a Trajectory instance.
        """
        openmp.omp_set_lock(&self.trajfile_lock)
        if self.is_trr:
            self.status = read_trr(self.trjfile, self.natoms, &self.step, &self.time, &self.lamb,
                                   boxptr, coordptr, velptr, forceptr)
            # Frame index
            self.t = (self.step - self.s0) // self.ds
            # Skip frames according to framestride
            while self.t % self.framestride != 0:
                self.status = read_trr(self.trjfile, self.natoms, &self.step, &self.time, &self.lamb,
                                       boxptr, coordptr, velptr, forceptr)
                self.t = (self.step - self.s0) // self.ds
        else:
            self.status = read_xtc(self.trjfile, self.natoms, &self.step, &self.time, boxptr, coordptr, &self.prec)
            self.t = (self.step - self.s0) // self.ds
            while self.t % self.framestride != 0:
                self.status = read_xtc(self.trjfile, self.natoms, &self.step, &self.time, boxptr, coordptr, &self.prec)
                self.t = (self.step - self.s0) // self.ds

        if self.status != exdrOK and self.status != self.file_end_status:
            raise IOError("Error while reading frame %i (%.3f ps)." % (self.t, self.time))
        t_ptr[0] = self.t // self.framestride
        openmp.omp_unset_lock(&self.trajfile_lock)

        return self.status


    cdef int write_frame(self, int natoms, int step, float t, float lamb, matrix b, rvec* x, rvec* v, rvec* f) except -1:
        """
        Write frame data if trajectory file is in writing mode.
        Currently only works for trr files.
        """
        assert self.is_trr, 'Only trr writing is supported'
        assert self.mode == 'wb', 'Trajectory is not in writing mode'
        self.status = write_trr(self.trjfile, natoms, step, t, lamb, b, x, v, f)
        if self.status != exdrOK:
            raise IOError("Error while writing .trr frame")
        return self.status


    cdef int rewrite_step(self, Trajectory traj_out, int write_velocities, int write_forces) except -1:
        """
        Rewrite trajectory to trajectory object traj_out  with renumbered frame indices.
        """
        self.open()           # open input file
        traj_out.open()       # open output file
        #printf("Writing from input trajectory %s with %d atoms to output trajectory %s with %d atoms", self.filename, self.natoms, traj_out.filename, traj_out.natoms)
        for i in range(self.nframes):
            self.status = read_trr(self.trjfile, self.natoms, &self.step, &self.time, &self.lamb,
                                   self.b, self.x, self.v, self.f)
            if self.status != exdrOK:
                raise IOError("Error while reading .trr")
            self.step = i
            if not write_velocities:
                self.v = NULL
            if not write_forces:
                self.f = NULL
            traj_out.write_frame(self.natoms, self.step, self.time, self.lamb, self.b, self.x, self.v, self.f)
        self.close()
        traj_out.close()
        return 0


    cdef int rewrite_time(self, Trajectory traj_out, int write_velocities, int write_forces, float dt, int timeprec) except -1:
        """
        Rewrite trajectory to trajectory object traj_out  with the time starting from 0
        with time step dt rounded to timeprec figures.
        """
        self.open()
        traj_out.open()
        ti = 0.
        for _ in range(self.nframes):
            self.status = read_trr(self.trjfile, self.natoms, &self.step, &self.time, &self.lamb,
                                   self.b, self.x, self.v, self.f)
            if self.status != exdrOK:
                raise IOError("Error while reading .trr")
            if not write_velocities:
                self.v = NULL
            if not write_forces:
                self.f = NULL
            self.time = ti
            traj_out.write_frame(self.natoms, self.step, self.time, self.lamb, self.b, self.x, self.v, self.f)
            ti = round(ti + dt, timeprec)  # add dt after writing current time
        self.close()
        traj_out.close()


    def open(self):
        """
        Opens XDRFILE in mode self.mode under self.trjfile.
        """
        #print("Opening trajectory file '%s' of type '%s' in mode '%s'" % (self.filename, self.ftype, self.mode))
        self.trjfile = xdrfile_open(self.filename.encode("UTF-8"), self.mode.encode("UTF-8"))


    def close(self):
        """
        Closes XDRFILE self.trjfile.
        """
        #print("Closing trajectory file '%s' of type '%s' in mode '%s'" % (self.filename, self.ftype, self.mode))
        xdrfile_close(self.trjfile)
        return


    def __del__(self):
        openmp.omp_destroy_lock(&self.trajfile_lock)
        self.close()
        PyMem_Free(self.x)
        PyMem_Free(self.v)
        PyMem_Free(self.f)


