# cython: boundscheck=True, wraparound=True, nonecheck=True, language_level=3str

cimport cython
from time import monotonic


cdef class Logger:
    """
    Basic logger.
    Stores walltime
    Writes messages to log file and stdout (by calling the object).
    Or writes just to file with write(msg).
    Filename is the base name of -o with .log extension. Fallback is -f.
    """
    def __init__(self, args):
        """ args: dict from argument parser """
        self.starttime = monotonic()
        fnm = args.get('o')
        if fnm is None:
            fnm = args.get('f')
        fnm = fnm.rsplit(".", 1)[0] + '.log'
        self.logfile = open(fnm, "w")
        print("Writing to log file '%s'" % fnm)


    def __call__(self, msg):
        """
        Writes string to file and stdout.
        """
        print(msg)
        self.logfile.write(msg + '\n')


    def write_args(self, args):
        """
        Write key-value pair of argument dict.
        """
        for i, j in args.items():
            self.__call__("-"+ str(i).ljust(15) + ": " + str(j))


    def finalize(self):
        """
        Write walltime and close file.
        """
        endtime = monotonic()
        wtime = endtime - self.starttime
        self.__call__("\nWalltime: %.1f s | %.1f min | %.2f h" % (wtime, wtime / 60, wtime / 3600))
        self.logfile.close()


    def __del__(self):
        self.logfile.close()

