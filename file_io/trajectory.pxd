# cython: boundscheck=False, wraparound=False, nonecheck=False, language_level=3str

cimport openmp
from common.common cimport real, rvec, matrix

# Import headers from xdrlib (for reading .xtc and .trr)
cdef extern from "file_io/xdrfile/xdrfile.h":
    ctypedef struct XDRFILE

cdef class Trajectory:
    cdef:
        float begin, end, t0, dt_frame, dt, time, prec, lamb
        int natoms, s0, ds, skip, natoms_gro, step, status, end_is_set, endstep, is_trr, framestride, file_end_status
        long t, nframes, offs
        matrix b
        rvec* x
        rvec* v
        rvec* f
        XDRFILE* trjfile
        openmp.omp_lock_t trajfile_lock
        str mode, filename, ftype
        list grohead, atoms, boxsize
        dict args

    cdef int _read_frame_serial(self, int* stepptr, matrix boxptr, rvec* coordptr, rvec* velptr, rvec* forceptr) except -1
    cdef int read_frame(self, long* t_ptr, matrix boxptr, rvec* coordptr, rvec* velptr, rvec* forceptr) nogil except -1
    cdef int write_frame(self, int natoms, int step, float t, float lamb, matrix b, rvec* x, rvec* v, rvec* f) except -1
    cdef int rewrite_step(self, Trajectory traj_out, int write_velocities, int write_forces) except -1
    cdef int rewrite_time(self, Trajectory traj_out, int write_velocities, int write_forces, float dt, int timeprec) except -1

