#!/usr/bin/env python3
"""
Setup for Cython modules. Usage: ./setup.py install
Defaults to inplace installation, see setup.cfg for details.
"""

from setuptools import Extension, setup
from Cython.Distutils import build_ext
from Cython.Build import cythonize
import Cython.Compiler.Options
import numpy

Cython.Compiler.Options.annotate = True  # Generate Cython html files

ext_modules = [
    Extension("common.common",
             ["common/common.pyx"],
              extra_link_args=['-fopenmp'],
             ),
    Extension("common.selection",
             ["common/selection.pyx"],
             ),
    Extension("ana.correlations",
             ["ana/correlations.pyx"],
              extra_link_args=['-fopenmp', '-lfftw3'],
             ),
    Extension("file_io.file_io",
             ["file_io/file_io.pyx"],
             ),
    Extension("file_io.logger",
             ["file_io/logger.pyx"],
             ),
    Extension("file_io.trajectory",
                [
                 "file_io/trajectory.pyx", "file_io/xdrfile/xdrfile.c",
                 "file_io/xdrfile/xdrfile_xtc.c", "file_io/xdrfile/xdrfile_trr.c"
                ],
              extra_link_args=['-fopenmp'],
             ),
    Extension("template.template",
             ["template/template.pyx"],
              extra_compile_args=["-fopenmp"],
              extra_link_args=['-fopenmp'],
             ),
        ]

setup(
        cmdclass={"install": build_ext},
        include_dirs=['.', numpy.get_include()],
        ext_modules=cythonize(ext_modules),
     )
