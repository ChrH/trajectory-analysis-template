# Trajectory Analysis Template
This is a template for analyzing molecular dynamics trajectories in the GROMACS .xtc and .trr binary formats. It is written in Cython/Python 3 and supports thread-parallel analysis using OpenMP.

It includes the xdrfile library (v. 1.1.4) by David van der Spoel and Erik Lindahl, which is used to read the .xtc and .trr file formats and is distributed under the BSD 2-Clause License (see e.g. file_io/xdrfile/xdrfile.h).

## Why Cython ?
Cython provides easy access to both the performance of C with efficient multithreading as well as the comfort of high-level Python libraries. Cython also has an easy way to interpret C arrays as numpy arrays without having to copy any data: \
http://docs.cython.org/en/latest/src/userguide/memoryviews.html

## Requirements
- Linux only (not tested on other OS)
- python-dev or python-devel (name depends on the distribution)
- gcc >= 7 
- Python >= 3.7
- Cython >= 0.29
- NumPy >= 1.16.0
- FFTW with source (libfftw3 and libfftw3-dev) >= 3.3.8

## Installation
Install with:
`./setup.py install`

Additional arguments for the compiler and linker can be provided in setup.py with `extra_compile_args` and `extra_link_args` separately for each module.

If the option `Cython.Compiler.Options.annotate = True` is set in setup.py, Cython generates a .html file with the resulting C code for profiling and debugging. 

## Functionality and Examples
This template is a simple example for the analysis of hydrogen bonds in water.
It calculates the full water-water H-bond existence matrix, where H-bonds are determined via a mixed angle-distance criterion that can not be calculated with the GROMACS tool `gmx hbond`.
For a publication using this criterion, see e.g.\
https://doi.org/10.1039/C5CP03069B.

Basic usage:
`template.py -f trajectory.xtc -s grofile.gro -sel SOL`

Options (available with `template.py -h`)
- `-f`: Name of the trajectory file (.xtc or .trr).
- `-s`: Name of a .gro file of the system.
- `-n`: Index file in the GROMACS .ndx format.
- `-o`: Output file name.
- `-q`: File containing the atom charges (see below).
- `-m`: File containing the atom masses (see below).
- `-sel`: Either the name of the residue in the .gro file, or, if an index file is provided with `-n`, the name or number of the index group.
- `-b`: First time in ps to analyze.
- `-e`: Last time to analyze.
- `-nt`: Number of threads to use.
- `-nframes`: Number of frames to analyze. Note that if framestride > 1, this reduces the maximum number of frames.
- `-framestride`: Analyze frame if frame index t % framestride == 0.

#### Using -nframes
If `-nframes` is not set, the number of frames in the trajectory is determined by reading the trajectory file once before the analysis.
Setting `-nframes` saves time by skipping this first read, but the end of the trajectory is not detected, which may cause problems.
When combined with `-framestride`, `-nframes` is the number of frames to be analyzed, and does not count skipped frames.

#### Multithreading
The number of threads `-nt` is limited by the environment variable `OMP_NUM_THREADS`. All OpenMP environment variables work as usual for setting affinities with `OMP_PROC_BIND`, `OMP_PLACES`, etc.

#### Charge and Mass Files
The options `-q` and `-m` expect a file that contains atom name-value pairs for each selection, where selection sections are designated by `[ SELECTION_NAME ]`. As in GROMACS, charges are expected in units of *e* and masses in *u*. If multiple atoms whose names only differ by trailing digits have the same values, one entry with the base name is sufficient, e.g. the masses for atoms H1, H2, ... can all be set by the line `H 1.008`. Whenever an atom can not be found in the provided file, trailing digits are successively stripped from the atom name. If no entry is found, the files with default values under topology/default_charges.dat and topology/default_masses.dat are searched first for the given selection name, then under the selection name 'All'. 

## Signal Handling
Currently, the main loop does not respond to SIGINT (e.g. Ctrl-C), but others like SIGSTOP (Ctrl-Z), SIGTERM, and SIGKILL can be used to pause/terminate the program.

## Double Precision Trajectories
In order to analyze trajectories in double precision, the type of `real` needs to be changed from `float` to `double` in common/common.pxd, and the types of `rvec` and `matrix` need to be changed to `double` in file_io/xdrfile/xdrfile.h.
