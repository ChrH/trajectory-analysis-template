# cython: boundscheck=False, wraparound=False, nonecheck=False, language_level=3str
"""
Trajectory analysis template for GROMACS .trr and .xtc formats.
It calculates the full water-water H-bond existence matrix, where H-bonds are determined via a mixed
angle-distance criterion that can not be calculated with the GROMACS tool gmx hbond.
For a publication using this criterion, see e.g.
https://doi.org/10.1039/C5CP03069B.
Water molecules are detected from the name "SOL" in the .gro file, or can be selected
via index file in the GROMACS .ndx format. Since this example assumes a 3-point water model
(in atom order OHH), 4-point (etc.) models can be analyzed by creating an index group without the dummy atom(s).
"""


cimport openmp
cimport cython
from libc.stdio cimport *
from libc.stdlib cimport malloc, free
from libc.math cimport sqrt
from cython.parallel import prange, parallel, threadid
import numpy as np
from cpython.mem cimport PyMem_Malloc, PyMem_Free
import argparse

# Import from local modules file_io and common
from file_io.trajectory cimport Trajectory
#from file_io.file_io import read_gro, write_gro
from common.common cimport atom, pbc, dot, calc_dipole, calc_com, assign_coordinates_charges_masses_to_atoms
#from common.common cimport assign_velocities_to_atoms, assign_forces_to_atoms
from common.common import startup
from common.common cimport real, rvec, matrix  # ctypedefs
from common.selection cimport Selection
from ana.correlations cimport FFTCorrelationHandler


@cython.cdivision(True)
cdef int analyze_frame(Trajectory traj, Selection sel, rvec* allcoords, rvec* velocities, rvec* forces,
                       atom* atoms_sel, rvec* com, rvec* mu, unsigned short* hbond_matrix, int* Nhbonds) nogil except -1:
    cdef:
        matrix b
        rvec box, boxh
        int i, j, k, offs_i, offs_j
        long t
        double d_Hi_Oj, d_Hi_Oi, cos_angle
        rvec Hi_Oj, Hi_Oi
        int* water_H_index = [1, 2]

    # Read frame from .trr/.xtc (uses openmp lock)
    # 't' is already the correct index for indexing, frames skipped due to framestride are not counted
    traj.read_frame(&t, b, allcoords, velocities, forces)

    # Output array offset from frame index
    offset = sel.num_mol * t

    # Box vectors from diagonals of 3x3 box matrix
    for i in range(3):
        box[i] = b[i][i]
        boxh[i] = box[i] / 2.0

    # Assign data to atom structs:
    # There are sel.num_mol elements in atoms_sel
    # Each atom has attributes (if set):
    # r (coordinate vector, e.g. atoms_sel[i].r[0] is the x-component of the position of atom i)
    # q (charge)
    # m (mass)

    # Last 2 arguments: 1 if charges / masses should be assigned, else 0
    assign_coordinates_charges_masses_to_atoms(sel, box, allcoords, atoms_sel, 1, 1)
    #assign_velocities_to_atoms(sel, velocities, atoms_sel)
    #assign_forces_to_atoms(sel, forces, atoms_sel)

    # Get all water COMs and dipole moments (not used in this example)
    for i in range(sel.num_mol):
        # Index for first atom of this molecule: i * sel.mol_size
        calc_com(&atoms_sel[i * sel.mol_size], sel.mol_size, sel.mol_mass, box, boxh, com[i])
        calc_dipole(&atoms_sel[i * sel.mol_size], sel.mol_size, box, boxh, mu[i])

    # Initialize H-bond existence matrix
    for i in range(sel.num_mol ** 2):
        hbond_matrix[i] = 0

    # Main loop for calculating hbonds
    for i in range(sel.num_mol):
        offs_i = sel.mol_size * i
        for j in range(sel.num_mol):
            if i == j:
                continue
            offs_j = sel.mol_size * j
            for k in range(2):
                pbc(atoms_sel[offs_i + water_H_index[k]].r, atoms_sel[offs_j].r, box, boxh, Hi_Oj)
                d_Hi_Oj = dot(Hi_Oj, Hi_Oj)
                # Maximum possible value for distance between donor-H and acceptor in an H-bond is 0.308**2 = 0.094864
                if d_Hi_Oj < 0.0949:
                    # Internal bond vector Oi-Hi:
                    pbc(atoms_sel[offs_i + water_H_index[k]].r, atoms_sel[offs_i].r, box, boxh, Hi_Oi)
                    d_Hi_Oi = dot(Hi_Oi, Hi_Oi)
                    cos_angle = dot(Hi_Oi, Hi_Oj) / sqrt(d_Hi_Oi * d_Hi_Oj)
                    # Check if Hi donates H-bond to Oj
                    if sqrt(d_Hi_Oj) < -0.171 * cos_angle + 0.137:
                        Nhbonds[t] += 1
                        hbond_matrix[i * sel.num_mol + j] = 1
                        # Make H-bond matrix symmetric, since distinction between donated and accepted H-bonds is not necessary
                        hbond_matrix[j * sel.num_mol + i] = 1

    return 0


################### START
@cython.boundscheck(True)
@cython.wraparound(True)
@cython.nonecheck(True)
def main():

    # Argument parser
    parser = argparse.ArgumentParser(description="Template: Water Hbonds")
    parser.add_argument('-f', required=True, help='Trajectory (.xtc) file.')
    parser.add_argument('-s', required=True, help='.gro file')
    parser.add_argument('-n', default=None, help='.ndx file (optional)')
    parser.add_argument('-m', default=None, help='File with atom masses. Format: see topology/default_masses.dat')
    parser.add_argument('-q', default=None, help='File with atom charges. Format: see topology/default_charges.dat')
    parser.add_argument('-o', default='nhbonds.xvg', help='Output file name (no extension or .xvg).')
    parser.add_argument('-b', type=float, help='Start time for analysis in ps.')
    parser.add_argument('-e', type=float, help='End time for analysis in ps.')
    parser.add_argument('-sel', default='SOL', help='Selection. Either residue name in gro, or name of index group, or number of index group.')
    parser.add_argument('-nt', type=int, default=0, help='Number of OpenMP threads.')
    parser.add_argument('-nframes', type=int, help='Optional: Number of trajectory frames to analyze after -b (including boundaries). Faster, but does not check for end of trajectory.')
    parser.add_argument('-framestride', type=int, default=1, help='Set to only analyze every -framestride frames.')
    args = vars(parser.parse_args())

    # Initialization: Clean up input args, print details to screen, and check/set number of OpenMP threads.
    # returns the new args and a Logger object
    args, logger = startup(args)

    cdef:
        long t
        Selection selection1
        int nt = args.get('nt')
        Trajectory traj = Trajectory(args) # init trajectory
        # Options from args can be overwritten:
        #Trajectory traj = Trajectory(args, f='traj.trr', s='struct.gro')

    # Find molecules in selection by initializing Selection object:
    # Gets atom names from gro, masses and charges from -m/-q files or default values.
    selection1 = Selection(args.get('sel'), args)
    # If the charges and/or masses are not needed for all atoms, the requirement can be switched off.
    # When no charge/mass can be found for an atom, it is set to 0 and a warning is printed:
    #selection1 = Selection(args.get('sel'), args, require_charges=False, require_masses=False)

    # Determine number of frames to analyze, open trajectory file, and skip frames if necessary
    traj.get_nframes()
    traj.open()
    traj.skip_frames()

    # Declare and init result arrays
    cdef int* Nhbonds = <int*> PyMem_Malloc(traj.nframes * sizeof(int))
    for i in range(traj.nframes):
        Nhbonds[i] = 0

    # Define thread-local arrays
    cdef:
        rvec* allcoords
        rvec* velocities
        rvec* forces
        atom* atoms_sel
        rvec* com
        rvec* mu
        unsigned short* hbond_matrix

    with nogil, parallel(num_threads=nt):
        # Allocate thread-local arrays
        allcoords = <rvec*> malloc(traj.natoms * sizeof(rvec))
        velocities = <rvec*> malloc(traj.natoms * sizeof(rvec))
        forces = <rvec*> malloc(traj.natoms * sizeof(rvec))
        atoms_sel = <atom*> malloc(selection1.size * sizeof(atom))
        com = <rvec*> malloc(selection1.num_mol * sizeof(rvec))
        mu = <rvec*> malloc(selection1.num_mol * sizeof(rvec))
        hbond_matrix = <unsigned short*> malloc(selection1.num_mol ** 2 * sizeof(unsigned short))

        # Parallelized loop
        for t in prange(traj.nframes, schedule='static', chunksize=1):
            analyze_frame(traj, selection1, allcoords, velocities, forces, atoms_sel, com, mu, hbond_matrix, Nhbonds)
            if (t + 1) % 1000 == 0:
                printf("Frame %10ld/%10ld\r", t + 1, traj.nframes)
                fflush(stdout)

        free(allcoords)
        free(velocities)
        free(forces)
        free(atoms_sel)
        free(com)
        free(mu)
        free(hbond_matrix)

    # If you want to use python functions in analyze_frame, it is no longer parallelizable.
    # Remove the "with nogil, parallel()" context manager. Then the main loop is:
    #for t in range(traj.nframes):
    #   analyze_frame(traj, selection1, allcoords, velocities, forces, atoms_sel, com, mu, hbond_matrix, Nhbonds)

    printf("Frame %10ld/%10ld\n", traj.nframes, traj.nframes)
    printf("Trajectory is done.\n")
    fflush(stdout)

    ###############################################
    # Calculate the (not normalized) autocorrelation function of Nhbonds
    cdef:
        # Create handler object for calculating correlation functions using FFT convolutions.
        FFTCorrelationHandler FFTCH = FFTCorrelationHandler(nt=1, size=traj.nframes, pad_pow2=True)
        int tid
        double* acf = <double*> PyMem_Malloc(FFTCH.size * sizeof(double))
        double* Nhbonds_double = <double*> PyMem_Malloc(FFTCH.size * sizeof(double))

    # Zero autocorrelation function array.
    # Since FFTCH only works with type double, create Nhbonds array of that type.
    for i in range(FFTCH.size):
        acf[i] = 0
        Nhbonds_double[i] = Nhbonds[i]

    # The correlation handler can be used to calculate multiple correlation functions in parallel
    # inside a prange loop within the nogil, parallel block.
    # IMPORTANT: FFTCH.autocorrelate and crosscorrelate ADD the result to the output. This
    # process is thread-safe, so all threads may add to the same output array for later averaging.
    # In the case of a single correlation function, the nogil, parallel block and prange loop are not necessary.
    with nogil, parallel(num_threads=1):
        tid = threadid()
        # Calculate autocorrelation.
        for t in prange(1, schedule='static', chunksize=1):
            FFTCH.autocorrelate(tid, Nhbonds_double, acf)

    # Unbias the correlation function (divide by number of samples at each delay):
    FFTCH.unbias_correlation_function(acf)


    ###############################################
    # Write Nhbonds time series to file
    if args.get('b') is not None:
        out_time = args.get('b')
    else:
        out_time = traj.t0
    with open("%s.xvg" % (args['o']), "w") as of:
        for i in range(traj.nframes):
            of.write("%.4f %i\n" % (out_time, Nhbonds[i]))
            out_time = round(out_time + traj.dt, 4)

    # Write autocorrelation function:
    out_time = 0
    with open("acf_%s.xvg" % (args['o']), "w") as of:
        for i in range(traj.nframes):
            of.write("%.4f %.7e\n" % (out_time, acf[i]))
            out_time = round(out_time + traj.dt, 4)


    ###############################################
    # Create numpy array by coercion of a C array without having to copy the data.
    # Details: http://docs.cython.org/en/latest/src/userguide/memoryviews.html
    Nhbonds_np = np.asarray(<int[:traj.nframes]> Nhbonds)

    avhb_tot = np.mean(Nhbonds_np)
    avhb = np.mean(Nhbonds_np) * 2 / selection1.num_mol

    # Free memory
    PyMem_Free(Nhbonds)

    # Print information and write it to log file
    logger("Number of frames: %i" % traj.nframes)
    logger("Average number of hydrogen bonds in the system: %f" % avhb_tot)
    logger("Average number of hydrogen bonds per water: %f" % avhb)
    logger.finalize()

