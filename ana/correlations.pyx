# cython: boundscheck=False, wraparound=False, nonecheck=False, language_level=3str

cimport openmp
from math import ceil
from libc.stdlib cimport malloc, calloc, free
from libc.string cimport memset, memcpy
from libc.math cimport log2


cdef class FFTCorrelationHandler():
    """
    This class is used to calculate 1-D auto- and crosscorrelation functions using the convolution theorem.
    It supports multi-threaded use by creating separate buffers and fftw_plan objects for each thread.
    The number of threads and input length are set at initialization and are fixed.
    Multiple correlations can be run in parallel by using a prange loop and providing the thread IDs from OpenMP
    to the autocorrelate and crosscorrelate methods.
    IMPORTANT: the autocorrelate and crosscorrelate methods ADD the correlation function in a thread-safe way
    to the output array.

    Theory:
    For real-valued functions f(t) and g(t) and Fourier transforms F(f(t)), the correlation function is
    the convolution with one function inverted along the t-axis (* is the convolution operation):
    CCF = f(t)*g(-t) = F^-1(F(f(t)).conj(F(g(t))))
    conj is the complex conjugate, which can be used because f and g are real:
    conj(F(f(t))) = F(f(-t))

    By using FFT for the Fourier transforms, the time complexity for correlating inputs of length N is
    improved from O(N^2) to O(N log N).

    Output structure of Fourier transforms using FFTW (with length 2N):
    The first element is the zero frequency. The next N are the positive frequencies.
    After that, the N-1 negative frequencies are in reverse order, i.e. decreasing frequencies (increasing absolute value).
    The highest frequency appears only in the positive frequency range.
    """

    def __init__(self, nt=1, size=None, pad_pow2=True, planner_flag=64):
        """
        Inputs:
        nt:           Number of threads.
        size:         Input signal length without zero padding.
        pad_pow2:     Round up array size to the next power of 2, which is usually much faster.
        planner_flag: Planning mode, see FFTW documentation or fftw3.h. Default is FFTW_ESTIMATE (64).
                      Other options are FFTW_MEASURE (0), FFTW_PATIENT (32), etc.
        """

        assert self.size is not None, "Missing argument 'size' in FFTCorrelationHandler initialization"
        self.nt = nt
        self.size = size
        self.size_padded = 2 * self.size
        if pad_pow2:
            self.size_padded = 2 ** ceil(log2(self.size_padded))
        self.norm = 1. / self.size_padded
        self.planner_flag = planner_flag
        self._alloc()
        openmp.omp_init_lock(&self.lock)


    def _alloc(self):
        """
        Allocate buffers and create FFTW plans for all transforms for each thread.
        """
        self.buf_in_forward1 = <complex*> calloc(self.nt * self.size_padded, sizeof(complex))
        self.buf_in_forward2 = <complex*> calloc(self.nt * self.size_padded, sizeof(complex))
        self.buf_in_backward = <complex*> calloc(self.nt * self.size_padded, sizeof(complex))
        self.buf_out_forward1 = <complex*> calloc(self.nt * self.size_padded, sizeof(complex))
        self.buf_out_forward2 = <complex*> calloc(self.nt * self.size_padded, sizeof(complex))
        self.buf_out_backward = <complex*> calloc(self.nt * self.size_padded, sizeof(complex))
        self.plan_forward1 = <fftw_plan*> malloc(self.nt * sizeof(fftw_plan))
        self.plan_forward2 = <fftw_plan*> malloc(self.nt * sizeof(fftw_plan))
        self.plan_backward = <fftw_plan*> malloc(self.nt * sizeof(fftw_plan))

        for i in range(self.nt):
            # -1 and 1 are the exponents of the transform. -1 is forward, +1 is backward.
            self.plan_forward1[i] = fftw_plan_dft_1d(self.size_padded, &self.buf_in_forward1[i * self.size_padded],
                                                     &self.buf_out_forward1[i * self.size_padded], -1, self.planner_flag)
            self.plan_forward2[i] = fftw_plan_dft_1d(self.size_padded, &self.buf_in_forward2[i * self.size_padded],
                                                     &self.buf_out_forward2[i * self.size_padded], -1, self.planner_flag)
            self.plan_backward[i] = fftw_plan_dft_1d(self.size_padded, &self.buf_in_backward[i * self.size_padded],
                                                     &self.buf_out_backward[i * self.size_padded],  1, self.planner_flag)


    cdef void autocorrelate(self, int tid, double* inp, double* out) nogil:
        """
        Calculates autocorrelation function of input with size self.size.
        Adds resulting correlation function to output (thread-safe).
        tid: thread id
        """
        cdef int t
        cdef int offset = tid * self.size_padded

        for t in range(self.size):
            self.buf_in_forward1[offset + t] = inp[t]  # Converts from double to complex

        # Zero padding for elements size + 1, ..., size_padded
        # This has to be done every time because FFTW can destroy the inputs depending on the mode.
        memset(&self.buf_in_forward1[offset + self.size], 0, (self.size_padded - self.size) * sizeof(complex))

        # Forward transform
        fftw_execute(self.plan_forward1[tid])

        for t in range(self.size_padded):  # After transform, all size_padded elements can be nonzero
            self.buf_in_backward[offset + t] = self.buf_out_forward1[offset + t] * conj(self.buf_out_forward1[offset + t])

        fftw_execute(self.plan_backward[tid])

        self._write_result(&self.buf_out_backward[offset], out)

        return


    cdef void crosscorrelate(self, int tid, double* inp1, double* inp2, double* out) nogil:

        cdef int t
        cdef int offset = tid * self.size_padded

        for t in range(self.size):
            self.buf_in_forward1[offset + t] = inp1[t]
            self.buf_in_forward2[offset + t] = inp2[t]

        memset(&self.buf_in_forward1[offset + self.size], 0, (self.size_padded - self.size) * sizeof(complex))
        memcpy(&self.buf_in_forward2[offset + self.size], &self.buf_in_forward1[offset + self.size],
               (self.size_padded - self.size) * sizeof(complex))

        fftw_execute(self.plan_forward1[tid])
        fftw_execute(self.plan_forward2[tid])

        for t in range(self.size_padded):
            self.buf_in_backward[offset + t] = self.buf_out_forward1[offset + t] * conj(self.buf_out_forward2[offset + t])

        fftw_execute(self.plan_backward[tid])

        self._write_result(&self.buf_out_backward[offset], out)

        return


    cdef void _write_result(self, complex* inp, double* out) nogil:
        """
        Normalize the real part of the backwards transform (i.e. correlation function)
        and add it from 'inp' to 'out' in a thread-safe way.
        """
        cdef int t

        openmp.omp_set_lock(&self.lock)
        for t in range(self.size):
            out[t] += creal(inp[t]) * self.norm
        openmp.omp_unset_lock(&self.lock)

        return


    cdef void unbias_correlation_function(self, double* inp) nogil:
        """
        Longer delays have fewer samples due to finite time series length.
        Divide each delay by the number of samples at that delay.
        """
        cdef int t

        for t in range(self.size):
            inp[t] /= (self.size - t)

        return


    def __del__(self):
        openmp.omp_destroy_lock(&self.lock)

        free(self.buf_in_forward1)
        free(self.buf_in_forward2)
        free(self.buf_in_backward)
        free(self.buf_out_forward1)
        free(self.buf_out_forward2)
        free(self.buf_out_backward)

        for i in range(self.nt):
            fftw_destroy_plan(self.plan_forward1[i])
            fftw_destroy_plan(self.plan_forward2[i])
            fftw_destroy_plan(self.plan_backward[i])

