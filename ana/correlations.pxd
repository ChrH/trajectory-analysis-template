# cython: boundscheck=False, wraparound=False, nonecheck=False, language_level=3str

cimport openmp


cdef extern from "complex.h":
    double creal(double complex z) nogil
    complex conj(double complex z) nogil


cdef extern from "fftw3.h":
    ctypedef struct fftw_plan_struct:
        pass
    ctypedef fftw_plan_struct* fftw_plan
    fftw_plan fftw_plan_dft_1d(int n, complex* inp, complex* out, int sign, unsigned flags) nogil
    void fftw_execute(fftw_plan plan) nogil
    void fftw_destroy_plan(fftw_plan plan) nogil


cdef class FFTCorrelationHandler:
    cdef:
        int nt, size, size_padded, planner_flag
        double norm
        openmp.omp_lock_t lock
        complex* buf_in_forward1
        complex* buf_in_forward2
        complex* buf_in_backward
        complex* buf_out_forward1
        complex* buf_out_forward2
        complex* buf_out_backward
        fftw_plan* plan_forward1
        fftw_plan* plan_forward2
        fftw_plan* plan_backward

    cdef void autocorrelate(self, int tid, double* inp, double* out) nogil
    cdef void crosscorrelate(self, int tid, double* inp1, double* inp2, double* out) nogil
    cdef void _write_result(self, complex* inp, double* out) nogil
    cdef void unbias_correlation_function(self, double* inp) nogil

