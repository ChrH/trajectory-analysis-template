#!/usr/bin/env python3
"""
    This is just a wrapper for the Cython module 'template'.
    For the Cython source see the template/template.pyx file.
"""

from template.template import main

if __name__=='__main__':
    main()
