# cython: boundscheck=False,wraparound=False,nonecheck=False,language_level=3str

cdef class Selection:
    cdef:
        groupindex
        str name
        list atomnames
        list distinct_atomnames
        list atomindices
        list moleculeindices
        list index
        list mol_mass_list
        list num_atoms_mol_list
        list atommasses
        list atomcharges
        dict indexdict
        float mol_mass
        float mol_charge
        float* atom_masses
        float* atom_charges
        int size
        int mol_size
        int num_mol
        int* atom_indices
        int* mol_indices
