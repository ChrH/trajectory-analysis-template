# cython: boundscheck=False, wraparound=False, nonecheck=False, language_level=3str

cimport cython
cimport openmp
from libc.stdlib cimport malloc, free
from libc.math cimport sqrt
import os.path
import sys
from common.selection cimport Selection
from file_io.logger import Logger


cdef struct atom:
    rvec r
    rvec v
    rvec f
    float mass
    float q
    #int index   # absolute atom index in .gro
    #char* name


cdef void pbc(real* v1, real* v2, real* box, real* boxh, real* v1v2) nogil:
    """
    Calculates the vector from v1 to v2 with periodic boundary conditions.
    """
    cdef int i
    v1v2[0] = v2[0] - v1[0]
    v1v2[1] = v2[1] - v1[1]
    v1v2[2] = v2[2] - v1[2]

    if v1v2[0] < -1 * boxh[0]:
            v1v2[0] += box[0]
    elif v1v2[0] > boxh[0]:
            v1v2[0] -= box[0]

    if v1v2[1] < -1*boxh[1]:
            v1v2[1] += box[1]
    elif v1v2[1] > boxh[1]:
            v1v2[1] -= box[1]

    if v1v2[2] < -1 * boxh[2]:
            v1v2[2] += box[2]
    elif v1v2[2] > boxh[2]:
            v1v2[2] -= box[2]
    return


cdef void assign_coordinates_charges_masses_to_atoms(Selection sel, rvec box, rvec* allcoords,
                                                     atom* atoms_sel, int do_charges, int do_masses) nogil:
    """
    Assign coorrdinates, charges, and masses to atom structs.
    """
    cdef int i, j
    for i in range(sel.size):
        for j in range(3):
            atoms_sel[i].r[j] = allcoords[sel.atom_indices[i]][j]
            while atoms_sel[i].r[j] < 0:
                atoms_sel[i].r[j] += box[j]
            while atoms_sel[i].r[j] > box[j]:
                atoms_sel[i].r[j] -= box[j]
        if do_charges:
            atoms_sel[i].q = sel.atom_charges[i]
        if do_masses:
            atoms_sel[i].mass = sel.atom_masses[i]
    return


cdef void assign_velocities_to_atoms(Selection sel, rvec* velocities, atom* atoms_sel) nogil:
    cdef int i, j
    for i in range(sel.size):
        for j in range(3):
            atoms_sel[i].v[j] = velocities[sel.atom_indices[i]][j]
    return


cdef void assign_forces_to_atoms(Selection sel, rvec* forces, atom* atoms_sel) nogil:
    cdef int i, j
    for i in range(sel.size):
        for j in range(3):
            atoms_sel[i].f[j] = forces[sel.atom_indices[i]][j]
    return


@cython.cdivision(True)
cdef void calc_dipole(atom* atoms, int num_atoms, real* box, real* boxh, real* dipole) nogil:
    """
    Calculate dipole moment of uncharged group.
    """
    cdef:
        int i, j, k
        real dist
        atom* atoms_buf = <atom*> malloc(num_atoms * sizeof(atom))
    for j in range(3):
        for i in range(num_atoms):
            atoms_buf[i].r[j] = atoms[i].r[j]
        dipole[j] = 0
        for i in range(num_atoms):
            for k in range(i):
                dist = atoms_buf[i].r[j] - atoms_buf[k].r[j]
                if dist > boxh[j]:
                    atoms_buf[i].r[j] -= box[j]
                elif dist < -1 * boxh[j]:
                    atoms_buf[k].r[j] -= box[j]
        for i in range(num_atoms):
            dipole[j] += atoms_buf[i].r[j] * atoms[i].q
    free(atoms_buf)
    return


cdef unsigned int is_hbonded(atom D, atom H, atom A, real* box, real* boxh, double cos_cut, double r_cut) nogil:
    """
    Luzar/Chandler H-bond criterion check for a DH-A H-bond.
    """
    cdef atom D_A, D_H
    cdef double d_D_A, d_D_H
    pbc(D.r, A.r, box, boxh, D_A.r)
    d_D_A = dot(D_A.r, D_A.r)
    if d_D_A < r_cut:    # 0.35**2 = 0.1225
        pbc(D.r, H.r, box, boxh, D_H.r)
        d_D_H = dot(D_H.r, D_H.r)
        cos_angle = dot(D_A.r, D_H.r)
        cos_angle /= sqrt(d_D_A * d_D_H)
        if cos_angle > cos_cut: # cos(30 * pi / 180) = 0.866
            return 1
        return 0


cdef double dot(real* v1, real* v2) nogil:
    cdef int i
    cdef double dist = 0
    for i in range(3):
        dist += v1[i] * v2[i]
    return dist


cdef void cross(real* v1, real* v2, real* v1xv2) nogil:
    v1xv2[0] = v1[1] * v2[2] - v1[2] * v2[1]
    v1xv2[1] = v1[2] * v2[0] - v1[0] * v2[2]
    v1xv2[2] = v1[0] * v2[1] - v1[1] * v2[0]
    return


@cython.cdivision(True)
cdef void calc_com(atom* atoms, int num_atoms, float molecule_mass, real* box, real* boxh, real* com) nogil:
    """
    Calculates molecular center of mass.
    """
    cdef:
        int i, j, k
        real dist
        atom* atoms_buf = <atom*> malloc(num_atoms * sizeof(atom))
    for j in range(3):
        for i in range(num_atoms):
            atoms_buf[i].r[j] = atoms[i].r[j]
        com[j] = 0
        for i in range(num_atoms):
            for k in range(i):
                dist = atoms_buf[i].r[j] - atoms_buf[k].r[j]
                if dist > boxh[j]:
                    atoms_buf[i].r[j] -= box[j]
                elif dist < -1 * boxh[j]:
                    atoms_buf[k].r[j] -= box[j]
        for i in range(num_atoms):
            com[j] += atoms[i].mass * atoms_buf[i].r[j]
        com[j] /= molecule_mass
        if com[j] < 0:
            com[j] += box[j]
        elif com[j] > box[j]:
            com[j] -= box[j]
    free(atoms_buf)
    return


def startup(args):
    """
    Print information, modify some args, check if provided input files exist, and set number of threads.
    Input: args (dict)
    Output: args (dict), logger (Logger)
    """
    # Print program name and all arguments
    length = 100
    logger = Logger(args)
    logger("#" * length + "\n" + "#" + sys.argv[0].center(length - 2) + "#\n" + "#" * length + "\n")
    logger("Command line arguments:")
    logger.write_args(args)
    logger("")

    # Remove output file extension
    if args.get('o') is not None:
        args['o'] = args['o'].replace(".xvg", "")

    # Check if files exist:
    files = ['f', 's', 'n', 'q', 'm']
    for i in files:
        fnm = args.get(i)
        if fnm is not None and not os.path.isfile(fnm):
            raise IOError("File %s not found." % fnm)

    # Get the number of threads
    MAX_OMP_THREADS = openmp.omp_get_max_threads() # number of allowed threads, controlled with environment variable OMP_NUM_THREADS
    logger("Maximum number of OpenMP threads: %i" % MAX_OMP_THREADS)
    args['nt'] = args.get('nt', 1)  # If -nt does not exist for some reason, use 1 thread.
    if args.get('nt') == 0:
        args['nt'] = MAX_OMP_THREADS
    assert args.get('nt') <= MAX_OMP_THREADS, "-nt is greater than OMP_NUM_THREADS (%i)" % MAX_OMP_THREADS
    logger("Using %i OpenMP threads.\n" % args.get('nt'))

    return args, logger
