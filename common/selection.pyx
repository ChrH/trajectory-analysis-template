# cython: language_level=3str

from cpython.mem cimport PyMem_Malloc, PyMem_Free
import os.path
from file_io.file_io import read_ndx, read_propertyfile, read_gro


cdef class Selection:
    """
    Important attributes (generally assuming identical molecules/fragments in the selection):
    self.mol_size:     Number of atoms per molecule (int)
    self.num_mol:      Number of molecules in the selection (int)
    self.size:         Total rumber of atoms in the selection (int)
    self.mol_mass:     Molecular mass (float)
    self.mol_charge:   Effective charge per molecule (float)

    Arrays with self.size elements:
    self.atom_indices: Atom indices starting at 0, even though .gro starts at 1 (int)
    self.mol_indices:  Molecule/residue index of each atom starting at 0 (int)
    self.atom_masses:  Atom masses (float)
    self.atom_charges: Atom partial charges (float)
    """
    def __init__(self, name, args, require_charges=True, require_masses=True):
        """
        Input:
        name: Either selection name (from .gro), index group name, or index group number.
        args: Arguments from argument parser.
        """

        print("\n" + "#" * 100 + "\nReading selection '%s':\n" % name)

        # Get relevant file names
        masses_file = args.get('m')    # Name of file with atom masses
        charges_file = args.get('q')  # Name of file with atom charges
        head, gro, boxsize = read_gro(args.get('s'))
        ndx = args.get('n')

        self.name = name
        self.groupindex = None
        atomnames = []
        atomindices = []
        moleculeindices = []
        atommasses = []
        atomcharges = []
        mol_mass_list = []  # For checking whether all selected molecules have the same mass
        num_atoms_mol_list = []
        num_sel = 0         # Number of selected molecules
        num_atoms_mol = 0   # Number of atoms in current residue
        mol_mass = 0.
        mol_charge = 0.
        last_mol = None

        MassLookup = AtomPropertyLookup('mass', self.name)
        ChargeLookup = AtomPropertyLookup('charge', self.name)

        # Read index file (GROMACS format)
        if ndx is not None:
            self.indexdict = read_ndx(ndx)  # self.indexdict is dict[groupname][atomindex]
            print("Using atom order in index file")
            try:  # Is the -sel argument the group index ?
                self.groupindex = int(self.name)
                self.name = list(self.indexdict.keys())[self.groupindex]
                print("Converting group index '%i' to corresponding group name '%s'" % (self.groupindex, self.name))
            except ValueError:
                pass
            self.index = self.indexdict[self.name]
            print("\nSelected index group '%s'"%(self.name))
            atom_ind_gro = self.index  # use order in index file
        else:
            atom_ind_gro = list(range(1, len(gro) + 1))

        # Get relative path to default mass and charge files:
        dirpath, _ = os.path.split(__file__)  # __file__ is the full path to the file_io module
        top_path = os.path.realpath(os.path.join(dirpath, '..', 'topology'))
        # Read defaults
        default_charges_file = 'default_charges.dat'
        default_masses_file = 'default_masses.dat'
        default_charges = read_propertyfile(os.path.join(top_path, default_charges_file))
        default_masses = read_propertyfile(os.path.join(top_path, default_masses_file))
        # Read masses and charges from files if they were provided with -m and -q
        charges = read_propertyfile(charges_file)
        masses = read_propertyfile(masses_file)

        for i_gro in atom_ind_gro:  # order in .ndx file matters !
            i = i_gro - 1  # atom indexing in .ndx files starts at 1
            molind = int(gro[i][0]) - 1  # .gro indexing for residues starts at 1
            molname = gro[i][1].strip()
            aname = gro[i][2].strip()
            # Skip atom unless the molecule name is equal to selection or, if an index file is used, the atom index is in the index group.
            if ndx is None and not molname == self.name:
                continue

            # Since index group name can be different from residue name if index file is used,
            # the dicts for property lookup are redefined inside the loop.
            # Entries are in the order of lookup of properties.
            # keys are tuples of (selection name, file name)
            mass_dicts = {
                      (self.name, masses_file): masses.get(self.name),
                      (molname, masses_file): masses.get(molname),
                      (self.name, default_masses_file): default_masses.get(self.name),
                      (molname, default_masses_file): default_masses.get(molname),
                      ('all', default_masses_file): default_masses.get('all')
                    }
            charge_dicts = {
                      (self.name, charges_file): charges.get(self.name),
                      (molname, charges_file): charges.get(molname),
                      (self.name, default_charges_file): default_charges.get(self.name),
                      (molname, default_charges_file): default_charges.get(molname),
                      ('all', default_charges_file): default_charges.get('all')
                    }

            atomnames.append(aname)

            mass = MassLookup(aname, mass_dicts, required=require_masses)
            atommasses.append(mass)

            charge = ChargeLookup(aname, charge_dicts, required=require_charges)
            atomcharges.append(charge)

            moleculeindices.append(molind)
            atomindices.append(i)
            if molind != last_mol:
                num_sel += 1
                if last_mol is not None:
                    mol_mass_list.append(mol_mass)
                    num_atoms_mol_list.append(num_atoms_mol)
                mol_mass = 0
                mol_charge = 0.
                num_atoms_mol = 0
                last_mol = molind
            num_atoms_mol += 1
            mol_mass += mass
            mol_charge += charge

        mol_mass_list.append(mol_mass)
        num_atoms_mol_list.append(num_atoms_mol)

        self.mol_size = num_atoms_mol
        if self.mol_size == 0:
            raise ValueError("No atoms found for selection %s"%name)
        self.num_mol = num_sel
        self.size = sum(num_atoms_mol_list)  # self.mol_size * self.num_mol
        self.mol_mass = mol_mass
        self.mol_charge = mol_charge

        self.atom_indices = <int*> PyMem_Malloc(self.size * sizeof(int))
        self.mol_indices = <int*> PyMem_Malloc(self.size * sizeof(int))
        self.atom_masses = <float*> PyMem_Malloc(self.size * sizeof(float))
        self.atom_charges = <float*> PyMem_Malloc(self.size * sizeof(float))
        self.atomnames = atomnames
        self.distinct_atomnames = list(set(self.atomnames))
        for i in range(self.size):
            self.atom_indices[i] = atomindices[i]
            self.mol_indices[i] = moleculeindices[i]
            self.atom_masses[i] = atommasses[i]
            self.atom_charges[i] = atomcharges[i]
        # Check if molecular masses and number of atoms per molecule are the same.
        if any([mol_mass_list[0] != i for i in mol_mass_list[1:]]):
            print("\nWARNING: Molecular masses in the selection are not identical. Molecules are identified by their residue indices in the gro file. The centers of mass will be wrong. Be sure this is what you want.")
        if any([num_atoms_mol_list[0] != i for i in num_atoms_mol_list[1:]]):
            print("\nWARNING: Number of atoms per molecule in the selection are not identical. Molecules are identified by their residue indices in the gro file. Be sure this is what you want.")
        if abs(round(self.mol_charge, 8)) > 10**-6:  # Rounding is the only way i found to prevent underflow
            print("\nWARNING: Molecule charge is not zero! Net charge: %.16f" % self.mol_charge)
        print("\nSelection %s:\n%i molecules with %i atoms per molecule, a molecular mass of %.5f and a charge of %.8f."%(self.name, self.num_mol, self.mol_size, self.mol_mass, self.mol_charge))

        print("#" * 100 + "\n")
        return


    def __del__(self):
        PyMem_Free(self.atom_indices)
        PyMem_Free(self.mol_indices)
        PyMem_Free(self.atom_masses)
        PyMem_Free(self.atom_charges)


class AtomPropertyLookup():
    def __init__(self, propname, selname):
        """
        Inputs:
        propname: name of the property (str)
        selname:  name of the selection (str)
        """
        self.propname = propname
        self.selname = selname
        self.known = []


    def __call__(self, aname, dicts, required=True):
        """
        Try to find property value for aname.
        The method search_property_dict successively strips away trailing digits at the end of aname
        and tries to find the new name. When all digits are stripped and the name was not found,
        the same is repeated for the next dict in 'dicts'.

        Input:
        aname:    atom name (str)
        dicts:    dict of dicts that are searched for aname in order.
        required: If aname is not found in property dicts, throws error if True
                  or sets property to 0 and prints warning if False.
        """
        key, prop, aname_mod = self.property_lookup(aname, dicts)
        if required:
            assert prop is not None, "Could not find %s for atom '%s' with selection name '%s'" % (self.propname, aname, self.selname)
            if (aname, aname_mod, prop) not in self.known:
                self.known.append((aname, aname_mod, prop))
                print("Found %8s %7.4f for atom %4s (full name: %4s) in group '%s' of file '%s'"
                      % (self.propname, prop, aname_mod, aname, key[0], key[1]))
        elif prop is None:
            if (aname, aname_mod, None) not in self.known:
                self.known.append((aname, aname_mod, None))
                print("WARNING: Did not find %s for atom '%s' with selection name '%s', setting it to 0"
                      % (self.propname, aname, self.selname))
            prop = 0
        return prop


    def property_lookup(self, aname, dd):
        """
        Calls search_property_dict for each dict in input (dict of dicts),
        and returns the first non-None value along with
        the key of the dict it was found in and the potentially modified atom name.
        """
        for k, d in dd.items():
            val, aname_mod = self.search_property_dict(aname, d)
            if val is not None:
                return k, val, aname_mod
        return None, None, aname


    def search_property_dict(self, aname, d):
        """
        Find atom name or base name (with fewer/no trailing digits) in dictionary d.
        Otherwise return None.
        Always return the potentially modified atom name.
        """
        if d is None:
            return None, aname
        aname_mod = aname
        val = d.get(aname_mod)
        while val is None:
            if aname_mod[-1].isdigit():
                aname_mod = aname_mod[:-1]
                val = d.get(aname_mod)
            else:
                break
        return val, aname_mod
