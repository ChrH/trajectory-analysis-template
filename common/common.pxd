# cython: boundscheck=False,wraparound=False,nonecheck=False,language_level=3str

from common.selection cimport Selection

ctypedef float real    # For double precision: change float to double
ctypedef real[3] rvec
ctypedef real[3][3] matrix

cdef struct atom:
    rvec r
    rvec v
    rvec f
    float mass
    float q
    #int index  # absolute atom index in .gro
    #char* name

cdef void pbc(real* v1, real* v2, real* box, real* boxh, real* v1v2) nogil
cdef void assign_coordinates_charges_masses_to_atoms(Selection sel, rvec box, rvec* allcoords,
                                                     atom* atoms_sel, int do_charges, int do_masses) nogil
cdef void assign_velocities_to_atoms(Selection sel, rvec* velocities, atom* atoms_sel) nogil
cdef void assign_forces_to_atoms(Selection sel, rvec* forces, atom* atoms_sel) nogil
cdef void calc_dipole(atom* atoms, int num_atoms, real* box, real* boxh, real* dipole) nogil
cdef unsigned int is_hbonded(atom D, atom H, atom A, real* box, real* boxh, double cos_cut, double r_cut) nogil
cdef double dot(real* v1, real* v2) nogil
cdef void cross(real* v1, real* v2, real* v1xv2) nogil
cdef void calc_com(atom* atoms, int num_atoms, float molecule_mass, real* box, real* boxh, real* com) nogil
